"""Tests for path handling
"""

import pytest

from earthsnake.exc import ValidationError
from earthsnake.identity import Identity
from earthsnake.path import Path


@pytest.mark.parametrize(
    'path,error_msg',
    [
        pytest.param('/', 'Path length must be between 2 and 512', id='short'),
        pytest.param('/' + 'a' * 512, 'Path length must be between 2 and 512', id='long'),
        pytest.param('badstart', 'Paths must start with a /', id='no_slash_start'),
        pytest.param('/@atstart', 'Paths must not start with /@', id='slashat_start'),
        pytest.param('/double//slash', 'Paths must not contain //', id='double_slash'),
        pytest.param('/multiple!excl!', 'Only one ! is allowed in paths', id='multi_excl'),
        pytest.param('/test/', 'Paths must not end with a /', id='trailing_slash'),
    ],
)
def test_invalid(path: str, error_msg: str) -> None:
    """Test if validation fails for invalid paths"""

    with pytest.raises(ValidationError) as ctx:
        Path.validate(path)

    assert error_msg in str(ctx.value)


def test_invalid_non_ephemeral() -> None:
    """Test validating ephemeral paths when ephemeral is not allowed"""

    with pytest.raises(ValidationError) as ctx:
        Path.validate('/ephemeral!')

    assert 'Only ephemeral paths may contain !' in str(ctx.value)


def test_validate_ephemeral() -> None:
    """Test validation of ephemeral paths"""

    assert Path.validate('/ephemeral!', allow_ephemeral=True) is None


def test_is_ephemeral() -> None:
    """Test the is_ephemeral property"""

    assert Path('/ephemeral!').is_ephemeral
    assert not Path('/non-ephemeral').is_ephemeral


def test_is_shared() -> None:
    """Test the is_shared property"""

    assert Path('/test').is_shared
    assert not Path('/test/~').is_shared


@pytest.mark.parametrize(
    'author,path,allowed',
    [
        pytest.param(
            '@test.bcz76z52y5dlpohtkmpuj3jsdcvfmebzpcgfmtmhu4u7hlexzreya',
            '/test',
            True,
            id='shared',
        ),
        pytest.param(
            '@test.bcz76z52y5dlpohtkmpuj3jsdcvfmebzpcgfmtmhu4u7hlexzreya',
            '/test/~@test.bcz76z52y5dlpohtkmpuj3jsdcvfmebzpcgfmtmhu4u7hlexzreya',
            True,
            id='allowed',
        ),
        pytest.param(
            '@test.bcz76z52y5dlpohtkmpuj3jsdcvfmebzpcgfmtmhu4u7hlexzreya',
            '/test/~@abob.bcz76z52y5dlpohtkmpuj3jsdcvfmebzpcgfmtmhu4u7hlexzreya',
            False,
            id='someone_else',
        ),
        pytest.param(
            '@test.bcz76z52y5dlpohtkmpuj3jsdcvfmebzpcgfmtmhu4u7hlexzreya',
            '/test/~',
            False,
            id='strictly_forbidden',
        ),
    ],
)
def test_can_write(author: str, path: str, allowed: bool) -> None:
    """Test the can_write method"""

    path_obj = Path(path)
    identity = Identity.from_address(author)

    assert path_obj.can_write(identity) is allowed


def test_str() -> None:
    """Test the __str__ method"""

    assert str(Path('/test.txt')) == '/test.txt'


def test_repr() -> None:
    """Test the __repr__ method"""

    assert repr(Path('/test.txt')) == '<Path /test.txt>'


def test_startswith() -> None:
    """Test the startswith method"""

    path = Path('/test.txt')

    assert path.startswith('/test')
    assert not path.startswith('test')


def test_endswith() -> None:
    """Test the endswith method"""

    path = Path('/test.txt')

    assert path.endswith('.txt')
    assert not path.endswith('.py')


def test_eq_other_path() -> None:
    """Test if Paths can be compared to other Paths"""
    assert Path('/test.txt') == Path('/test.txt')
    assert Path('/test.txt') != Path('/test.md')


def test_eq_str() -> None:
    """Test if Paths can be compared to strings"""

    assert Path('/test.txt') == '/test.txt'
    assert Path('/test.txt') != '/test.md'


def test_lt() -> None:
    """Test if Paths can be sorted with other Paths (< direction)"""

    assert Path('/test.txt') < Path('/very.txt')
    assert not Path('/test.txt') < Path('/much.txt')  # pylint: disable=unneeded-not


def test_lt_str() -> None:
    """Test if Paths can be sorted with strings (< direction)"""

    assert Path('/very.txt') > '/test.txt'
    assert not Path('/much.txt') > '/test.txt'  # pylint: disable=unneeded-not


def test_gt() -> None:
    """Test if Paths can be sorted with other Paths (> direction)"""

    assert Path('/very.txt') > Path('/test.txt')
    assert not Path('/much.txt') > Path('/test.txt')  # pylint: disable=unneeded-not


def test_g_str() -> None:
    """Test if Paths can be sorted with strings (> direction)"""

    assert Path('/very.txt') > '/test.txt'
    assert not Path('/much.txt') > '/test.txt'  # pylint: disable=unneeded-not
