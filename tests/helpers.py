"""Helper functions for tests
"""

from random import choice
from typing import Any, Dict

from earthsnake.types import ALPHA_LOWER, ALPHA_LOWER_OR_DIGIT

DEFAULT_IDENTITY_SEED = (
    b'\xe5:\xc9\x95$\x9d\xc5F\xee\xe6\x84\xbe\xcc\xda^\xc4'
    b'z\x84\xb7\xd2\x02q\xfa\xe8W\xd8z\x05E\xfb2\xd5'
)

VALID_DOCUMENT: Dict[str, Any] = {
    'format': 'es.4',
    'timestamp': 1651738871668993,
    'deleteAfter': None,
    'author': '@test.bcz76z52y5dlpohtkmpuj3jsdcvfmebzpcgfmtmhu4u7hlexzreya',
    'path': '/test.txt',
    'workspace': '+test.suffix',
    'signature': (
        'bughac3ooy5qfect4bxdke2zkun2wqufhpivt57vj2mzq52mhqzesjvhyywttxm7qqjyhoskmiqd2lw72qy2u766r'
        'fqvnbwab4qp3gbi'
    ),
    'content': 'test',
    'contentHash': 'bt6dnbamijr6wlgrp5kqmkwwqcwr36ty3fmfyelgrlvwblmhqbiea',
}


def random_name() -> str:
    """Generate a valid random author name"""

    return choice(ALPHA_LOWER) + ''.join(choice(ALPHA_LOWER_OR_DIGIT) for _ in range(3))
