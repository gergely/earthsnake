"""Tests for the Share class"""

import pytest

from earthsnake.types import ALPHA_LOWER, ALPHA_LOWER_OR_DIGIT
from earthsnake.exc import ValidationError
from earthsnake.share import Share


@pytest.mark.parametrize(
    'name,suffix,error',
    [
        pytest.param('80smusic', 'suffix', 'Invalid name "80smusic"', id='name_digitstart'),
        pytest.param('a.b', 'suffix', 'Invalid name "a.b"', id='name_period'),
        pytest.param('PARTY', 'suffix', 'Invalid name "PARTY"', id='name_uppercase'),
        pytest.param('test', 'b.c', 'Invalid suffix "b.c"', id='suffix_period'),
        pytest.param('test', '4ever', 'Invalid suffix "4ever"', id='suffix_digitstart'),
        pytest.param('test', 'TIME', 'Invalid suffix "TIME"', id='suffix_uppercase'),
    ],
)
def test_create_invalid(name: str, suffix: str, error: str) -> None:
    """Test if share creation fails with invalid values"""

    with pytest.raises(ValidationError) as ctx:
        Share(name, suffix=suffix)

    assert str(ctx.value) == error


def test_create() -> None:
    """Test if creating a share with a name and a suffix succeeds"""

    share = Share('test', suffix='suffix')

    assert share.name == 'test'
    assert share.suffix == 'suffix'


def test_create_no_suffix() -> None:
    """Test if creating succeeds if no suffix is given, and the suffix is randomly chosen"""

    share = Share('test')

    assert share.name == 'test'
    assert len(share.suffix) == 53


def test_str() -> None:
    """Test the __str__ method"""

    share = Share('test', 'suffix')

    assert str(share) == '+test.suffix'


def test_repr() -> None:
    """Test the __repr__ method"""

    share = Share('test', 'suffix')

    assert repr(share) == '<Share +test.suffix>'


@pytest.mark.parametrize(
    'address',
    (
        pytest.param('test.suffix', id='no_prefix'),
        pytest.param('+t.st.suffix', id='invalid_name_char'),
    ),
)
def test_validate_invalid(address: str) -> None:
    """Test if validate_address fails if the address is invalid"""

    with pytest.raises(ValidationError) as ctx:
        Share.validate_address(address)

    assert str(ctx.value) == f'Invalid share address {address}'


@pytest.mark.parametrize(
    'address',
    (
        pytest.param('test.suffix', id='no_prefix'),
        pytest.param('+t.st.suffix', id='invalid_name_char'),
    ),
)
def test_from_address_invalid(address: str) -> None:
    """Test if from_address fails if the address is invalid"""

    with pytest.raises(ValidationError) as ctx:
        Share.from_address(address)

    assert str(ctx.value) == f'Invalid share address {address}'


def test_from_address() -> None:
    """Test constructing a share from a string address"""

    share = Share.from_address('+test.suffix')

    assert share.name == 'test'
    assert share.suffix == 'suffix'


@pytest.mark.parametrize('length', range(1, 54))
def test_generate_suffix(length: int) -> None:
    """Test the random suffix generator"""

    suffix = Share.generate_random_suffix(length=length)

    assert suffix[0] in ALPHA_LOWER
    assert all(char in ALPHA_LOWER_OR_DIGIT for char in suffix)
    assert len(suffix) == length
