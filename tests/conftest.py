"""Test configuration and global fixtures
"""

from nacl.signing import SigningKey
import pytest
from _pytest.fixtures import SubRequest

from earthsnake.document.es4 import Es4Document
from earthsnake.identity import Identity

from .helpers import DEFAULT_IDENTITY_SEED, VALID_DOCUMENT, random_name


@pytest.fixture
def identity(request: SubRequest) -> Identity:
    """A valid identity"""

    seed = b''
    name = random_name()

    for marker in request.node.iter_markers('id_key_seed'):
        for seed in marker.args:
            pass

    seed = seed or DEFAULT_IDENTITY_SEED

    for marker in request.node.iter_markers('id_name'):
        for name in marker.args:
            pass

    sign = SigningKey(seed)

    return Identity(name, sign_key=sign)


@pytest.fixture
def es4_document() -> Es4Document:
    """A valid es.4 document"""

    return Es4Document.from_json(VALID_DOCUMENT)
