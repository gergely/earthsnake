from typing import Any, Callable, Dict


def compile(definition: Dict[str, Any], handlers: Dict[str, Any] = ..., formats: Dict[str, Any] = ..., use_default: bool = ...) -> Callable[[Any], None]: ...
