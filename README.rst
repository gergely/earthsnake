Python implementation of Earthstar
##################################

**WARNING!**

This library is very limited in functionality, and is under active development.  Be prepared for
things to break.

Usage
=====

Create an identity and save it as a mnemonic:

.. code-block:: python

   from earthsnake import Identity

   identity = Identity('test')
   mnemonic = identity.mnemonic

Then you can load it back from the mnemonic:

.. code-block:: python

   identity = Identity.from_mnemonic(mnemonic)

Development
===========

Type checking:

.. code-block:: sh

   env MYPYPATH=mypy-stubs poetry run mypy --strict earthsnake tests

Static linting:

.. code-block:: sh

   poetry run pylint earthsnake tests

Code style check:

.. code-block:: sh

   poetry run black --diff --skip-string-normalization earthsnake tests

Tests:

.. code-block:: sh

   poetry run pytest -vv --cov=. tests
