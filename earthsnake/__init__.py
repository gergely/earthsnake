"""Earthsnake - An Earthstar implementation in Python
##################################################

Usage example
=============

.. code-block:: python

   import earthsnake

   identity = earthsnake.Identity('test')
   mnemonic = identity.mnemonic
"""

__version__ = '0.1.0'

from .identity import Identity
from .document import Document
