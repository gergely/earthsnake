"""Exception types for Earthsnake
"""


class EarthsnakeError(Exception):
    """Basic Earthsnake error

    Should not be used except as a base type.
    """


class ValidationError(EarthsnakeError):
    """Raised when something doesn’t pass as a valid Earthsnake object"""
