"""Document handling classes"""

from abc import ABC, abstractmethod
from datetime import datetime
from typing import Optional

from ..identity import Identity
from ..path import Path


class Document(ABC):  # pragma: no cover pylint: disable=too-few-public-methods
    """Abstract base class for documents"""

    path: Path

    author: Identity

    timestamp: datetime

    signature: Optional[str]

    @property
    @abstractmethod
    def content_length(self) -> int:
        """The length of the document"""

    @abstractmethod
    def sign(self, identity: Optional[Identity] = None) -> None:
        """Sign the document as identity

        If ``identity`` is ``None``, use ``self.author`` instead.
        """

    def __repr__(self) -> str:
        return f'<{self.__class__.__name__} {self.path} by {self.author}>'
